> theJungle

YAMR - Yet Another Malware Repository

# Rules

``
samples/
  UID
    [sha1.1]/
      [sha1.1].sha1
      [sha1.1].sha256
      [sha1.1].ssdeep
      [sha1.1].password → “infected”
      [sha1.1].zip
        [sha1.1].malware
    [sha1.2]/
      [sha1.2].sha1
      [sha1.2].sha256
      [sha1.2].ssdeep
      [sha1.2].password → “infected”
      [sha1.2].zip
        [sha1.2].malware
  info/
    README.md --> Indice de familias de malware por UID
    UID/
      README.md → Info del malware {
        [sha1.1] es ...
        [sha1.2] es ...
      }
      names.txt → Nombres de ficheros {
        [sha1.1]:file.exe
        [sha1.2]:file.dll
      }
      rules/ → Reglas de detección
```

1. Crear carpeta UID en samples e info
2. Archivar el binario (hashes, zip cifrado, etc.)
3. Añadir ficheros en info/UID (README, names y directorio rules)
4. Si es una familia nueva, identificar en info/README.md

# Index

- 
